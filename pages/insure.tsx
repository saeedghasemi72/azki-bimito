import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";
import { Formik, Form } from "formik";
import { Col, Row, Spin } from "antd";
import { useQueryClient } from "react-query";

import * as yup from "yup";

import { QueryInsureCompanies } from "query";
import { Select, Button, Submit } from "components";

const validationSchema = yup.object().shape({
  company: yup.string().required("نوع خودرو الزامی می باشد"),
});

type FormType = {
  company: string;
};

const Car: NextPage = () => {
  const router = useRouter();
  const { isLoading, data } = QueryInsureCompanies();
  const queryClient = useQueryClient();
  const insureDetails = queryClient.getQueryData("insure") as FormType;

  const handleSubmit = (formData: FormType, { setSubmitting }: any) => {
    queryClient.setQueryData("insure", formData);
    setSubmitting(false);
    router.push("/discounts");
  };

  return (
    <div>
      <Head>
        <title>Azki/Bimito</title>
      </Head>
      <h2 className="page-title">بیمه شخص ثالث</h2>
      <h3 className="page-dec">
        شرکت بیمه گر قبلی خود را در این بخش وارد کنید
      </h3>
      <Formik
        initialValues={
          insureDetails ?? {
            company: "",
          }
        }
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => (
          <Form>
            <Spin spinning={isLoading}>
              <Row gutter={[16, 16]}>
                <Col xs={24}>
                  <Select
                    formik={formik}
                    name="company"
                    options={(data || []).map((el: any) => ({
                      key: el?.title,
                      title: el?.title,
                    }))}
                  />
                </Col>
              </Row>
            </Spin>
            <div className="page__actions">
              <Submit formik={formik}>
                <Image
                  src="/image/arrow.svg"
                  width={10}
                  height={10}
                  alt="arrow"
                />
                <span>مرحله بعد</span>
              </Submit>
              <Link href="/car">
                <a>
                  <span>مرحله قبل</span>
                  <Image
                    src="/image/arrow.svg"
                    width={10}
                    height={10}
                    alt="arrow"
                  />
                </a>
              </Link>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

Car.getInitialProps = async (ctx: any) => {
  if (ctx.req) {
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();
    return {};
  } else {
    return {};
  }
};

export default Car;
