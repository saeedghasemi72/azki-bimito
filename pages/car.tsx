import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";
import { Formik, Form } from "formik";
import { Col, Row, Spin } from "antd";
import { useQueryClient } from "react-query";

import * as yup from "yup";

import { QueryGetCarType } from "query";
import { Select, Button, Submit } from "components";

const validationSchema = yup.object().shape({
  car: yup.string().required("نوع خودرو الزامی می باشد"),
  model: yup.string().required("مدل خودرو الزامی می باشد"),
});

type FormType = {
  car: string;
  model: string;
};

const Car: NextPage = () => {
  const router = useRouter();
  const { isLoading, data } = QueryGetCarType();
  const queryClient = useQueryClient();

  const carDetails = queryClient.getQueryData("car") as FormType;

  const handleSubmit = (formData: FormType, { setSubmitting }: any) => {
    queryClient.setQueryData("car", {
      ...formData,
      car: (data || [])
        .find((item: any) => item.id === 1)
        ?.brands.find((item: any) => item.id === formData.car)?.title,
    });
    setSubmitting(false);
    router.push("/insure");
  };

  const handleChangeMode = (formik: any) => {
    formik.setFieldValue("model", "");
  };
  return (
    <div>
      <Head>
        <title>Azki/Bimito</title>
      </Head>
      <h2 className="page-title">بیمه شخص ثالث</h2>
      <h3 className="page-dec">نوع و مدل خودروی خود را انتخاب کنید</h3>
      <Formik
        initialValues={
          carDetails ?? {
            car: "",
            model: "",
          }
        }
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => (
          <Form>
            <Spin spinning={isLoading}>
              <Row gutter={[16, 16]} className="column-reverse">
                <Col xs={24} md={12}>
                  <Select
                    formik={formik}
                    label="مدل خودرو"
                    name="model"
                    options={(
                      (data || [])
                        .find((item: any) => item.id === 1)
                        ?.brands.find(
                          (item: any) => item.id === formik.values.car
                        )?.models || []
                    )?.map((el: any) => ({
                      key: el?.title,
                      title: el?.title,
                    }))}
                  />
                </Col>
                <Col xs={24} md={12}>
                  <Select
                    formik={formik}
                    label="نوع خودرو"
                    name="car"
                    def
                    options={(
                      (data || []).find((item: any) => item.id === 1)?.brands ||
                      []
                    ).map((el: any) => ({
                      key: el?.id,
                      title: el?.title,
                    }))}
                    onChange={() => handleChangeMode(formik)}
                  />
                </Col>
              </Row>
            </Spin>
            <div className="page__actions">
              <Submit formik={formik}>
                <Image
                  src="/image/arrow.svg"
                  width={10}
                  height={10}
                  alt="arrow"
                />
                <span>مرحله بعد</span>
              </Submit>
              <Link href="/">
                <a>
                  <span>مرحله قبل</span>
                  <Image
                    src="/image/arrow.svg"
                    width={10}
                    height={10}
                    alt="arrow"
                  />
                </a>
              </Link>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

Car.getInitialProps = async (ctx: any) => {
  if (ctx.req) {
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();
    return {};
  } else {
    return {};
  }
};

export default Car;
