import { useState } from "react";
import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";
import { Formik, Form } from "formik";
import { Col, Row, Spin, Modal } from "antd";
import { useQueryClient } from "react-query";

import * as yup from "yup";

import { QueryThirdDiscount, QueryDriverDiscount } from "query";
import { Select, Button, Submit } from "components";

const validationSchema = yup.object().shape({
  thirdDiscount: yup.string().required("این فیلد الزامی می باشد"),
  driverDiscount: yup.string().required("این فیلد الزامی می باشد"),
});

type FormType = {
  thirdDiscount: string;
  driverDiscount: string;
};

type InsureType = {
  company: string;
};

type CarType = {
  car: string;
  model: string;
};

const Car: NextPage = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const { isLoading: isLoadingThirdDiscount, data: dataThirdDiscount } =
    QueryThirdDiscount();
  const { isLoading: isLoadingDriverDiscount, data: dataDriverDiscount } =
    QueryDriverDiscount();
  const queryClient = useQueryClient();

  const carDetails = queryClient.getQueryData("car") as CarType;
  const insureDetails = queryClient.getQueryData("insure") as InsureType;
  const discountDetails = queryClient.getQueryData("discount") as FormType;

  const handleSubmit = (formData: FormType, { setSubmitting }: any) => {
    queryClient.setQueryData("discount", formData);
    setSubmitting(false);
    toggleModal();
  };

  const toggleModal = () => {
    setIsModalVisible((prevState) => !prevState);
  };

  return (
    <div>
      <Head>
        <title>Azki/Bimito</title>
      </Head>
      <h2 className="page-title">بیمه شخص ثالث</h2>
      <h3 className="page-dec">
        درصد تخفیف بیمه شخص ثالث و حوادث راننده را وارد کنید
      </h3>
      <Formik
        initialValues={{
          thirdDiscount: "",
          driverDiscount: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => (
          <Form>
            <Spin spinning={isLoadingThirdDiscount || isLoadingDriverDiscount}>
              <Row gutter={[16, 16]} className="column-reverse">
                <Col xs={24} md={24}>
                  <Select
                    formik={formik}
                    name="thirdDiscount"
                    label="شخص ثالث"
                    options={(dataThirdDiscount || []).map((el: any) => ({
                      key: el?.title,
                      title: el?.title,
                    }))}
                  />
                </Col>
                <Col xs={24} md={24}>
                  <Select
                    formik={formik}
                    name="driverDiscount"
                    label="حوادث راننده"
                    options={(dataDriverDiscount || []).map((el: any) => ({
                      key: el?.title,
                      title: el?.title,
                    }))}
                  />
                </Col>
              </Row>
            </Spin>
            <div className="page__submit">
              <Submit formik={formik}>
                <Image
                  src="/image/arrow.svg"
                  width={10}
                  height={10}
                  alt="arrow"
                />
                <span>استعلام قیمت</span>
              </Submit>
            </div>
          </Form>
        )}
      </Formik>
      <Modal
        title="خلاصه اطلاعات وارد شده"
        visible={isModalVisible}
        onOk={toggleModal}
        onCancel={toggleModal}
        okText="تایید"
        cancelText="برگشت"
      >
        <p>
          <span>خودرو:</span>
          <span>{carDetails?.car}</span>
        </p>
        <p>
          <span>مدل خودرو:</span>
          <span>{carDetails?.model}</span>
        </p>
        <p>
          <span>شرکت بیمه گر قبلی:</span>
          <span>{insureDetails?.company}</span>
        </p>
        <p>
          <span>درصد تخفیف بیمه شخص ثالث:</span>
          <span>{discountDetails?.thirdDiscount}</span>
        </p>
        <p>
          <span>درصد تخفیف حوادث رانندگی:</span>
          <span>{discountDetails?.driverDiscount}</span>
        </p>
      </Modal>
    </div>
  );
};

Car.getInitialProps = async (ctx: any) => {
  if (ctx.req) {
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();
    return {};
  } else {
    return {};
  }
};

export default Car;
