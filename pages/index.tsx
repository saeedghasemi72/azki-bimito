import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useQueryClient } from "react-query";
import Head from "next/head";
import { Col, Row } from "antd";
import * as yup from "yup";
import { Formik, Form } from "formik";

import { Input, Submit, Layout } from "components";

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .required("نام الزامی می باشد")
    .matches(/^[\u0600-\u06FFs]+$/, "لطفا نام را به صورت فارسی وارد کنید"),
  lastName: yup
    .string()
    .required("نام خانوادگی الزامی می باشد")
    .matches(
      /^[\u0600-\u06FFs]+$/,
      "لطفا نام خانوادگی را به صورت فارسی وارد کنید"
    ),
  phone: yup
    .string()
    .required("شماره موبایل الزامی می باشد")
    .matches(
      /^(0|\+98)?([ ]|-|[()]){0,2}9[1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}/gi,
      "لطفا شماره تلفن معتبر وارد نمایید"
    ),
  password: yup
    .string()
    .required("رمز عبور الزامی می باشد")
    .min(4, "طول رمز عبور حداقل باید 4 باشد")
    .max(10, "طول رمز عبور حداکثر باید 10 باشد")
    .matches(/^(?=.*[A-Z])/, "رمز عبور باید شامل حداقل یک حرف بزرگ باشد")
    .matches(/^(?=.*[0-9])/, "رمز عبور باید شامل حداقل یک عدد باشد"),
});

type FormType = {
  lastName: string;
  name: string;
  phone: string;
  password: string;
};

const Home: NextPage = () => {
  const router = useRouter();
  const queryClient = useQueryClient();

  const handleSubmit = (formData: FormType, { setSubmitting }: any) => {
    queryClient.setQueryData("profile", formData);
    setSubmitting(false);
    router.push("/car");
  };

  const profile = queryClient.getQueryData("profile") as FormType;
  return (
    <div>
      <Head>
        <title>Azki/Bimito</title>
      </Head>
      <h2 className="page-title">ثبت نام</h2>
      <Formik
        initialValues={
          profile ?? {
            lastName: "",
            name: "",
            phone: "",
            password: "",
          }
        }
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => (
          <Form>
            <Row
              gutter={[
                { xs: 0, md: 16 },
                { xs: 0, md: 16 },
              ]}
            >
              <Col xs={24} md={12}>
                <Input formik={formik} name="lastName" label="نام خانوادگی" />
              </Col>
              <Col xs={24} md={12}>
                <Input formik={formik} name="name" label="نام" />
              </Col>
            </Row>
            <Input formik={formik} name="phone" label="شماره موبایل" />
            <Input
              formik={formik}
              type="password"
              name="password"
              label="رمز عبور"
            />
            <div className="page__submit">
              <Submit formik={formik}>ثبت نام</Submit>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Home;
