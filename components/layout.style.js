import styled, { css } from "styled-components";

export default styled.div(
  ({
    theme: {
      colors: { primary },
    },
  }) => css`
    position: relative;
    height: 100vh;
    .main {
      padding: 32px;
      position: relative;
      @media (max-width: 998px) {
        padding: 16px;
      }
      header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        .logo {
          width: 150px;
          text-align: right;
          .icon {
            width: 32px;
            svg {
              width: inherit;
            }
          }
        }
        .title {
          font-size: 18px;
          font-weight: bold;
          @media (max-width: 998px) {
            display: none;
          }
        }
        .profile {
          width: 150px;
          & > div {
            display: flex;
            justify-content: center;
            align-items: center;
            span:first-child {
              margin-right: 12px;
            }
          }
        }
      }
    }
    &:before {
      content: "";
      @media (min-width: 998px) {
        content: "";
        position: absolute;
        background-color: #fffbeb;
        left: 0;
        width: 30%;
        height: 100%;
      }
      @media (max-width: 998px) {
        content: "";
        position: absolute;
        background-color: #fffbeb;
        bottom: 0;
        width: 100%;
        height: 20%;
        z-index: -1;
      }
    }
    &:after {
      @media (min-width: 998px) {
        content: "";
        position: absolute;
        background-image: url("/image/car-green.svg");
        background-size: contain;
        background-repeat: no-repeat;
        left: 32px;
        bottom: 32px;
        width: 50%;
        height: 300px;
      }
      @media (max-width: 998px) {
        content: "";
        position: absolute;
        background-image: url("/image/car-green.svg");
        background-size: contain;
        background-repeat: no-repeat;
        left: 32px;
        bottom: 100px;
        width: 300px;
        height: 150px;
        z-index: -1;
      }
    }
    .content {
      width: 50%;
      margin: 0 0 0 auto;
      text-align: right;
      padding: 32px;
      @media (max-width: 998px) {
        width: 100%;
        padding: 16px;
      }
    }
  `
);
