import { Button as ButtonAntd } from "antd";
import PropTypes from "prop-types";

const Button = ({
  children,
  type,
  size = "md",
  className,
  label,
  ...params
}: any) => {
  return (
    <ButtonAntd
      className={`button ${className ?? ""}`}
      type={type}
      size={size}
      {...params}
    >
      {children || label}
    </ButtonAntd>
  );
};

export default Button;
