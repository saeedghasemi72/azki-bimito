import PropTypes from "prop-types";
import { Button } from "antd";

import StyleWrapper from "./form.style";

const Submit = ({
  children,
  className = "",
  formik,
  id = "submitBtn",
  type,
  label,
  size,
  ...rest
}: any) => {
  const { isSubmitting, isValid } = formik;

  return (
    <StyleWrapper className="submit-button">
      <Button
        className={className}
        htmlType="submit"
        id={id}
        data-test-id={id}
        loading={isSubmitting}
        disabled={!isValid}
        type={type}
        size={size}
        {...rest}
      >
        {children || label}
      </Button>
    </StyleWrapper>
  );
};

export default Submit;
