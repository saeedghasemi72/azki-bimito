import styled, { css } from "styled-components";

export default styled.section(
  ({
    theme: {
      colors: { bgRed, primary, light4, dark3 },
    },
  }) => css`
    margin-bottom: 16px;
    &.input {
      margin-bottom: 16px;
      position: relative;
    }

    input:-webkit-autofill {
      background-color: red !important;
    }

    .input:-internal-autofill-selected {
      background-color: transparent;
    }

    label {
      color: ${dark3};
      font-weight: 300;
      font-size: 14px;
      display: block;
      line-height: 1;
      padding-bottom: 3px;
    }

    .text-danger {
      padding: 2px 10px;
      border-radius: 8px;
      z-index: 1051;

      .text-danger--text {
        color: ${bgRed};
        font-size: 13px;
      }
    }

    &.select-input {
      .ant-select {
        height: 52px;
        width: 100%;

        .ant-select-selector {
          height: 52px;
          min-height: 52px;
          outline: none;
          box-shadow: none;
          /* border: none; */
          border-radius: 8px;
          background-color: ${light4};
          .ant-select-selection-search-input {
            height: 52px;
          }

          .ant-select-selection-placeholder {
            line-height: 52px;
          }

          .ant-select-selection-item {
            line-height: 52px;
          }
        }
      }
    }

    &.submit-button {
      button {
        border: none;
        outline: none;
        color: white;
        font-weight: 300;
        font-size: 12px;
        height: 32px;
        border-radius: 300px;
        background-color: ${primary};
        padding: 0 30px;
      }
    }
  `
);
