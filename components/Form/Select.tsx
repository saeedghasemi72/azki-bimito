import React from "react";
import { useField } from "formik";
import { Select } from "antd";

import StyleWrapper from "./form.style";

const SelectForm = ({ formik, label, options, onChange, ...props }: any) => {
  const [field, meta] = useField(props);

  const { setFieldValue } = formik;
  const { error, touched } = meta;
  const { name } = props;

  const handleChange = (option: any) => {
    setFieldValue(name, option);
    if (onChange) onChange(option);
  };

  return (
    <StyleWrapper className="input select-input">
      {label && <label htmlFor={name}>{label}</label>}

      <Select
        id={name}
        data-test-id={name}
        {...props}
        {...field}
        onChange={handleChange}
      >
        {options.map(({ key, title }: any) => (
          <Select.Option key={key} title={title} value={key}>
            {title}
          </Select.Option>
        ))}
      </Select>

      {touched && error && (
        <div className="text-danger">
          <span className="text-danger--text">{error}</span>
        </div>
      )}
    </StyleWrapper>
  );
};

export default SelectForm;
