import * as Path from "helpers/icons.helper";

type Props = {
  title: string;
  viewBox: string;
};

const SvgIcon = ({ title, viewBox }: Props) => {
  return (
    <span className={`icon icon-${title}`}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox={viewBox}
        style={{ verticalAlign: "middle" }}
      >
        {(Path as any)[title]}
      </svg>
    </span>
  );
};

export default SvgIcon;
