import { ReactChild } from "react";
import Image from "next/image";
import { useQueryClient } from "react-query";

import { SvgIcon } from "components";

import StyledWrapper from "./layout.style";

type Props = {
  children: ReactChild;
};

type profileType = {
  lastName: string;
  name: string;
  phone: string;
  password: string;
};

const Layout = ({ children }: Props) => {
  const queryClient = useQueryClient();
  const profile = queryClient.getQueryData("profile") as profileType;
  return (
    <StyledWrapper>
      <div className="main">
        <header>
          <div className="profile">
            {profile ? (
              <div>
                <span>{`${profile.name} ${profile.lastName}`}</span>
                <Image
                  src="/image/user.svg"
                  alt="user"
                  width={20}
                  height={20}
                />
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="title">سامانه مقایسه و خرید آنلاین بیمه</div>
          <div className="logo">
            <SvgIcon viewBox="0 0 17 17" title="LogoIcon" />
          </div>
        </header>

        <div className="content">{children}</div>
      </div>
    </StyledWrapper>
  );
};
export default Layout;
