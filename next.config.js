const withAntdLess = require("next-plugin-antd-less");

module.exports = withAntdLess({
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: "https://bimito.com/api/:path*",
      },
    ];
  },
});
