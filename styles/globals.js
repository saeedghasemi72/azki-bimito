import { createGlobalStyle, css } from "styled-components";

export const GlobalStyle = createGlobalStyle(
  ({
    theme: {
      colors: { primary },
    },
  }) => css`
    @font-face {
      font-family: "IRANSansWeb";
      src: url("public/fonts/iran.woff") format("woff2"),
        url("public/fonts/iran.woff2") format("woff");
    }

    body {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
      font-family: "IRANSansWeb";
    }
    .page-title {
      font-weight: bold;
      font-size: 24px;
      margin-bottom: 24px;
      @media (max-width: 998px) {
        text-align: center;
      }
    }
    .page-dec {
      font-size: 14px;
      opacity: 0.8;
      margin-bottom: 24px;
      @media (max-width: 998px) {
        text-align: center;
      }
    }
    .ant-select-item-option-content {
      text-align: right;
    }
    .page__actions {
      display: flex;
      justify-content: space-between;
      align-items: center;
      button,
      a {
        background-color: transparent !important;
        border: 1px solid ${primary} !important;
        color: ${primary} !important;
        display: flex;
        justify-content: space-between;
        align-items: center;
        span {
          margin: 0 5px;
        }
      }
      .submit-button {
        margin: 0;
      }
      a {
        padding: 0 30px;
        border-radius: 300px;
        height: 32px;
        img {
          transform: rotate(180deg);
        }
      }
    }
    .page__submit {
      text-align: left;
    }
    .ant-modal-body {
      text-align: right;
    }
    .ant-modal-close {
      left: 0;
      right: unset !important;
    }
    .ant-modal-title {
      text-align: right;
      width: 100%;
    }
    .ant-btn-primary {
      background-color: ${primary};
    }
    .column-reverse {
      @media (max-width: 998px) {
        flex-direction: column-reverse;
      }
    }
  `
);
