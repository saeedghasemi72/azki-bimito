import * as api from "api";
import { useMutation, useQuery } from "react-query";

export const QueryGetCarType = () => {
  return useQuery("carType", api.GetCarType, {
    staleTime: Infinity,
  });
};

export const QueryInsureCompanies = () => {
  return useQuery("insureCompanies", api.GetInsureCompanies, {
    staleTime: Infinity,
  });
};

export const QueryThirdDiscount = () => {
  return useQuery("ThirdDiscount", api.GetThirdDiscount, {
    staleTime: Infinity,
  });
};

export const QueryDriverDiscount = () => {
  return useQuery("driverDiscount", api.GetDriverDiscount, {
    staleTime: Infinity,
  });
};
