import axios from "axios";

const client = axios.create({
  baseURL: "/api",
});

export const GetCarType = async () => {
  const { data } = await client.get("/product/vehicle/models/third");
  return data;
};

export const GetInsureCompanies = async () => {
  const { data } = await client.get("/product/third/companies");
  return data;
};

export const GetThirdDiscount = async () => {
  const { data } = await client.get("/product/third/third-discounts");
  return data;
};

export const GetDriverDiscount = async () => {
  const { data } = await client.get("/product/third/driver-discounts");
  return data;
};
